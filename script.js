$(document).ready(function() {
  var scroll_pos = 0;
  $(document).scroll(function() {
    scroll_pos = $(this).scrollTop();
    if (scroll_pos > 0) {
      $('.navbar-custom').removeClass('nav-clear').addClass('nav-red');
    } else {
      $('.navbar-custom').removeClass('nav-red').addClass('nav-clear');
    }
  });
});

function init_map() {
  var myOptions = {
    zoom: 13,
    center: new google.maps.LatLng(29.4952963, -98.39805669999998),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
  marker = new google.maps.Marker({
    map: map,
    position: new google.maps.LatLng(29.4952963, -98.39805669999998)
  });
  infowindow = new google.maps.InfoWindow({
    content: "<b>The Bargain Barn</b><br/>4734 Eisenhauer Rd<br/> San Antonio"
  });
  google.maps.event.addListener(marker, "click", function() {
    infowindow.open(map, marker);
  });
  infowindow.open(map, marker);
}
google.maps.event.addDomListener(window, 'load', init_map);